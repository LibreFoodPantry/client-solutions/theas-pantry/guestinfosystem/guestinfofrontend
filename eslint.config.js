import globals from "globals";
import pluginJs from "@eslint/js";
import pluginVue from "eslint-plugin-vue";
import eslintPluginJsonc from 'eslint-plugin-jsonc';


/** @type {import('eslint').Linter.Config[]} */
export default [
  {
    files: ["**/*.{js,mjs,cjs,vue}"]
  },

  {
    files: ["**/*.js"], languageOptions: {sourceType: "commonjs"}
  },

  {
    env: {
      es2021: true,
      node: true
    },

    languageOptions: {
      globals: {
        ...globals.browser,
        ...globals.node
      } 
    },

    rules: {
      "vue/max-attributes-per-line": ["warn", {
          "singleline": {
            "max": 10
          }
      }],
      "vue/singleline-html-element-content-newline": ["warn", {
          "ignoreWhenEmpty": true,
          "ignores": ["option", "h1", "h2", "h3", "h4", "small", "label", "p", "button"]
      }],
      "vue/html-self-closing": ["warn", {
          "html": {
            "void": "always",
            "normal": "always",
            "component": "always"
          },
          "svg": "always",
          "math": "always"
      }],
      "vue/no-mutating-props": ["error", {
          "shallowOnly": true
      }]  
    }
  },

  pluginJs.configs.recommended,
  ...pluginVue.configs["flat/essential"],
  ...eslintPluginJsonc.configs['flat/recommended-with-json']
];
