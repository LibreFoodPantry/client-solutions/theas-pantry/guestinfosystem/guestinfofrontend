import { createRouter, createWebHistory } from 'vue-router'

const router = createRouter({
  history: createWebHistory(import.meta.env.BASE_URL),
  routes: [
    { 
      path: '/', component: () => import('../layouts/MainLayout.vue'),
      children: [
        { path: '/', component: () => import('../views/LookUpPage.vue')},
        { path: '/home', component: () => import('../views/HomePage.vue')},
        { path: '/verify', component: () => import('../views/VerifyPage.vue') },
        { path: '/register', component: () => import('../views/RegisterPage.vue')}
      ]
    },
    
  ]
})

export default router
