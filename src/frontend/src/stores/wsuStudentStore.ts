import { defineStore } from 'pinia'
import router from '../router'
import { BACKEND_BASE_URL } from '../main'

// importing the router here isn't a good idea, instead return checkID status numbers...
// ...and deal with them in the login page. I'm only using the router here for proof of concept

export const databaseWarehouse = defineStore('wsuStudentStore', {
  // state is like data() method in vue app
  state: () => {
    return {
      guestData : {
        wsuID: 0,
        resident: true,
        zipCode: 0,
        unemployment: false,
        assistance: {
            socSec: false,
            TANF: false,
            finAid: false,
            other: false,
            SNAP: false,
            WIC: false,
            breakfast: false,
            lunch: false,
            SFSP: false
        },
        guestAge: 0,
        numberInHousehold: 1
      },
      existingGuest: false,
    }
  },
// Actions are like methods() in vue app
  actions: {
    validateData(guestDataToValidate) {
      const wsuID = guestDataToValidate.wsuID.toString()
      const wsuIDLength = wsuID.length

      // Test that wsuID length is 7 and only numbers 0-9
      if (wsuIDLength !== 7 || !/^\d+$/.test(wsuID)) {
        alert('WSU ID must be a 7-digit non-negative number between 0-9')
        return false
      }
      
      // We can do a cleaner version like this other than creating constants like in wsuID
      if (!guestDataToValidate.zipCode || guestDataToValidate.zipCode.toString().length !== 5 || !/^\d+$/.test(guestDataToValidate.zipCode)) {
        alert('Zip Code must be a 5-digit non-negative number')
        return false
      }

      // repeat cleaner version :)
      if (guestDataToValidate.guestAge < 18 || guestDataToValidate.guestAge > 50 || !Number.isInteger(guestDataToValidate.guestAge)) {
        alert('Guest Age must be a non-negative integer between 18 and 50')
        return false
      }

      // repeat cleaner version :)
      if (guestDataToValidate.numberInHousehold < 1 || guestDataToValidate.numberInHousehold > 10 || !Number.isInteger(guestDataToValidate.numberInHousehold)) {
        alert('Number in household cannot be less than 1 or more than 10')
        return false
      }

      // user entered valid data values
      return true
      
    },
    async checkID(userInput) {
      this.guestData.wsuID = userInput
			
      const studentIDLength = userInput.toString().length

      if (studentIDLength !== 7 || !/^\d+$/.test(userInput)) {
       alert('WSU ID length must be 7 and not include characters a-z')
        return
      }

      const guestURL = BACKEND_BASE_URL + '/guests/' + userInput
      console.log(guestURL);

      try {
        const response = await fetch(guestURL)

        if (response.status === 200) {
          // Existing user, needs to verify their information
          this.existingGuest = true
          
          router.push('/verify')

        } else if (response.status === 404) {
          if(confirm(userInput + ' not found. Please register first') === true) {
            // User clicked ok
            router.push('/register')
          }
          else {
            // User clicked cancel
            return
          }
        
        } else {
          console.log(response)
          console.error('Unexpected response status: ' + response.status)
          // Handle other status codes as needed
        }
      } catch (error) {
        console.error('An error occurred', error)
      }
    },
    async createGuest(newGuestData) {
      if(!this.validateData(newGuestData)) {
        // validate wsuID, zipCode, Age
        return
      }

      this.guestData = Object.assign({}, newGuestData)

      const guestURL = BACKEND_BASE_URL + '/guests/'

        try {
          let response = await fetch(guestURL, {
            method: 'POST',
            body: JSON.stringify(newGuestData),
            headers: { 
              'Accept': 'application/json',
              'Content-Type': 'application/json; charset=UTF-8'
              }
          })

          if (response.status === 201) {
            this.existingGuest = true
            let result = await response.json()
            //console.log(result)
            alert('Guest with ID ' + newGuestData.wsuID + ' is now registered')
            router.push('/verify')
          }

          else if (response.status === 409) {
            alert('Guest ' + newGuestData.wsuID +  ' already exists')
          }
          else {
            // We shouldn't be able to see this cause validation will terminate execution upon wrong format
            console.log(newGuestData)
            alert('Wrong data format. Please check your information and try again')
            throw new Error('An error occurred. Please try again')
          }
        } catch (error) {
            console.error('An error occurred:', error)
        }
    },
    async getGuestData(someWSUID) {
      const guestURL = BACKEND_BASE_URL + '/guests/' + someWSUID

      try {
        const response = await fetch(guestURL)

        if (!response.ok) {
          throw new Error('HTTP error! Status: ' + response.status)
          return
        }

        const data = await response.json()
        //console.log(data); // You can now use the 'data' variable to access the result
        return data
      } catch (error) {
        console.error('Error:', error)
      }

    },
    async updateGuestData(updatedGuestData) {
      if (!this.validateData(updatedGuestData)) {
       // alert('Invalid data. Please check your information and try again.')
        return false
      }
    
      const guestURL = BACKEND_BASE_URL + '/guests/' + updatedGuestData.wsuID
    
      try {
        const response = await fetch(guestURL, {
          method: 'PUT',
          body: JSON.stringify(updatedGuestData),
          headers: {
            'Accept': 'application/json',
            'Content-Type': 'application/json; charset=UTF-8'
          }
        })
      
        if (response.status === 200) {
          // Passing updatedGuestData to guestData state 
          this.guestData = Object.assign({}, updatedGuestData)
          const result = await response.json()
      
          alert('WSU ID ' + updatedGuestData.wsuID + ' info has been successfully updated.')
          return true
        } else if (response.status === 404) {
          alert('Please verify WSU ID')
          return false
        } else if (response.status === 400) { // 400 Bad request
          alert('Please enter correct format in all fields')
          return false
        } else {
          // Handle other status codes as needed
          alert('An unexpected error occurred. Please try again later.')
          return false
        }
      } catch (error) {
        console.error('An error occurred:', error)
        alert('An unexpected error occurred. Please try again later.')
        return false
      }
      
    }    
  },
  // getters are like computed values in vue
  getters: {
   // getStudentData: (state) => state.guestData
  },
  persist: true
})
