# Developer Cheat Sheet

**All commands listed here assume that you are working within the Dev Container.**
<!-- markdown-link-check-disable -->
* [Commands for developing the frontend itself in a hot-reloading development server](#commands-for-developing-the-frontend-itself-in-a-hot-reloading-development-server)
* [Commands for building the Docker image to serve the frontend](#commands-for-building-the-docker-image-to-serve-the-frontend)
* [Commands for running a test harness of backend, database, and message queue](#commands-for-running-a-test-harness-of-backend-database-and-message-queue)
* [Squash commits to prepare for merge into main](#squash-commits-to-prepare-for-merge-into-main))
<!-- markdown-link-check-enable -->

## Commands for developing the frontend itself in a hot-reloading development server

### Start the backend/database/message queue test harness

It is likely you will want the
<!-- markdown-link-check-disable-next-line -->
[backend/database/message queue test harness](#starting-a-locally-running-backenddatabasemessage-queue-test-harness)
running before starting the frontend.

### Running a frontend development server that hot-reloads changes

```bash
bin/frontend-dev-up.sh
```
<!-- markdown-link-check-disable-next-line -->
Open the development frontend at [http://localhost:5173/](http://localhost:5173)

It is likely you will also want the
<!-- markdown-link-check-disable-next-line -->
[backend/database/message queue test harness](#starting-a-locally-running-backenddatabasemessage-queue-test-harness)
running.

## Commands for building the Docker image to serve the frontend

### Build a new Docker image with NGINX and a newly built frontend

#### Running locally

Do this whenever you have modified files in your `src/frontend` directory
and want those changes reflected in the local Docker image for the frontend
server.

Generates a Docker image of the GuestInfoFrontend server.

**Use this command if you do not yet have a Docker image or you have modified**
**files in `src`.**

```bash
bin/frontend-prod-build.sh
```

The resulting *local* Docker image will be `guestinfofrontend:latest`

#### Running in the pipeline

The `bin/frontend-prod-build.sh` command is also run in the `build` stage of the
GuestInfoFrontend` pipeline.

The Docker image built by the pipeline will be stored in the GuestInfoFrontend
container registry. The tag for the image will depend on which branch the
pipeline is running on and/or Semantic Versioning rules.

### Start the backend/database/message queue test harness <!-- markdownlint-disable MD024 -->

It is likely you will want the
<!-- markdown-link-check-disable-next-line -->
[backend/database/message queue test harness](#starting-a-locally-running-backenddatabasemessage-queue-test-harness)
running before starting the frontend.

### Start a locally running GuestInfoFrontend server

To start the local Docker image:

```bash
bin/frontend-prod-up.sh
```

It is likely you will also want the
<!-- markdown-link-check-disable-next-line -->
[backend/database/message queue test harness](#starting-a-locally-running-backenddatabasemessage-queue-test-harness)
running.

### Stop a locally running GuestInfoFrontend server

To stop the local Docker image:

```bash
bin/frontend-prod-down.sh
```

### Restart a locally running GuestInfoFrontend server

To stop and restart the local Docker image:

```bash
bin/frontend-prod-restart.sh
```

## Commands for running a test harness of backend, database, and message queue

### Starting a locally running backend/database/message queue test harness

Starts a locally running GuestInfoFront server, along with GuestInfoBackend,
a MongoDB database, a RabbitMQ message queue, and networks to connect them.

This will continue to run, and generate logging messages, in your terminal
until it is terminated with Ctrl+C.

```bash
bin/backend-up.sh
```

### Stopping the locally running backend/database/message queue test harness

To stop the locally running test harness consisting of the backend, database,
and message queue connected with the correct networks:

Removes the Docker containers and networks.

The database will be emptied of all data.

```bash
bin/backend-down.sh
```

### Restarting the locally running backend/database/message queue test harness

To stop and restart the locally running test harness consisting of the
backend, database, and message queue connected with the correct networks:

Takes down Docker containers and networks and restarts the Docker
containers and networks.

**Use this command if you have not modified files in `src` and want to restart**
**the local server.** If you have modified files in `src` and want to restart
the server, use `commands/rebuild.sh` (see above).

This will continue to run, and generate logging messages, in your terminal
until it is terminated with Ctrl+C.

```bash
bin/backend-restart.sh
```

### Loading some data into the locally running backend/database/message queue test harness

To load some data into the locally running test harness consisting of the
backend, database, and message queue connected with the correct networks:

Loads some sample data into the backend server.

```bash
bin/backend-load-data.sh
```

## Squash commits to prepare for merge into main

Before merging a merge request, use the following command to squash its
commits into a single commit, writing a good conventional-commit message.

```bash
bin/premerge-squash.sh
```
