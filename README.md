# Guest Info System Frontend Server

The GuestInfoFrontend provides a web frontend that makes calls to
GuestInfoBackend (a REST API server that implements an OpenAPI
specification.) The users of the Thea's Pantry system are clients
of GuestInfoFrontend.

## Developer Guide

Getting Started

1. Read [LibreFoodPantry.org](https://librefoodpantry.org/)
    and join its Discord server.
2. [Install development environment](docs/developer/install-development-environment.md)
3. Clone this repository using the following command

    ```bash
    git clone <repository-clone-url>
    ```

4. Open it in VS Code and reopen it in a devcontainer.
5. This web frontend makes calls to [GuestInfoBackend](https://gitlab.com/LibreFoodPantry/client-solutions/theas-pantry/guestinfosystem/guestinfobackend)
   which implements the [GuestInfoAPI](https://gitlab.com/LibreFoodPantry/client-solutions/theas-pantry/guestinfosystem/guestinfobackend/-/blob/main/lib/openapi.yaml).
6. Familiarize yourself with the systems used by this project
  (see Development Infrastructure below).
7. See [the developer cheat-sheet](docs/developer/cheat-sheet.md) for common
  commands you'll likely use.

Development Infrastructure

* [Automated Testing](docs/developer/automated-testing.md)
* [Build System](docs/developer/build-system.md)
* [Continuous Integration](docs/developer/continuous-integration.md)
* [Dependency Management](docs/developer/dependency-management.md)
* [Development Environment](docs/developer/development-environment.md)
* [Documentation System](docs/developer/documentation-system.md)
* [Version Control System](docs/developer/version-control-system.md)
